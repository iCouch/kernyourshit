{
    "id": "814b78db-137f-4092-8ca1-2f1e7f4be39f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprButton",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 49,
    "bbox_left": 0,
    "bbox_right": 197,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3d1317ed-9637-499b-9aac-a6c677621629",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "814b78db-137f-4092-8ca1-2f1e7f4be39f",
            "compositeImage": {
                "id": "0262b3cf-7748-4d31-b23b-f18f103a3a75",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d1317ed-9637-499b-9aac-a6c677621629",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb6a1a4b-547c-4721-9604-8deb9e652d4a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d1317ed-9637-499b-9aac-a6c677621629",
                    "LayerId": "87656154-7c00-4ce5-87e8-b8e5bf871544"
                }
            ]
        },
        {
            "id": "77781e42-e293-4dc4-bcc3-d080338f72b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "814b78db-137f-4092-8ca1-2f1e7f4be39f",
            "compositeImage": {
                "id": "71405e86-a225-4546-985b-f411631962c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77781e42-e293-4dc4-bcc3-d080338f72b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "687135e1-595b-4dff-a894-f88106ef804e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77781e42-e293-4dc4-bcc3-d080338f72b6",
                    "LayerId": "87656154-7c00-4ce5-87e8-b8e5bf871544"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 50,
    "layers": [
        {
            "id": "87656154-7c00-4ce5-87e8-b8e5bf871544",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "814b78db-137f-4092-8ca1-2f1e7f4be39f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 198,
    "xorig": 99,
    "yorig": 25
}