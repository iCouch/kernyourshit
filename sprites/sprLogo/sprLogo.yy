{
    "id": "3c5930d9-c341-43f6-9125-f267a5634a0a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprLogo",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 206,
    "bbox_left": 0,
    "bbox_right": 280,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "96b8e3a0-b8b8-4e96-a510-173d25d0fdb3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c5930d9-c341-43f6-9125-f267a5634a0a",
            "compositeImage": {
                "id": "c6a86555-bb12-4895-a00f-f712a62b94a8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96b8e3a0-b8b8-4e96-a510-173d25d0fdb3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9adf97c5-23b8-445e-a074-6b9140f30cde",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96b8e3a0-b8b8-4e96-a510-173d25d0fdb3",
                    "LayerId": "619a6cf2-0d1f-4314-a4cf-660572a35973"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 207,
    "layers": [
        {
            "id": "619a6cf2-0d1f-4314-a4cf-660572a35973",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3c5930d9-c341-43f6-9125-f267a5634a0a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 281,
    "xorig": 140,
    "yorig": 103
}