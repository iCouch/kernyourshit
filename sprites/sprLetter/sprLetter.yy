{
    "id": "bc5e3b55-b8fd-4327-9507-2c20cecd91d2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprLetter",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2875f234-aee0-4ef2-b367-4430c97b30c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bc5e3b55-b8fd-4327-9507-2c20cecd91d2",
            "compositeImage": {
                "id": "de952e86-69a6-448b-bb89-cfd36bb97ac6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2875f234-aee0-4ef2-b367-4430c97b30c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa942a3b-b34d-4c34-8965-a2c3e75f2458",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2875f234-aee0-4ef2-b367-4430c97b30c1",
                    "LayerId": "ded2714d-14bf-4677-9814-f5a8b2fe7f06"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "ded2714d-14bf-4677-9814-f5a8b2fe7f06",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bc5e3b55-b8fd-4327-9507-2c20cecd91d2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}