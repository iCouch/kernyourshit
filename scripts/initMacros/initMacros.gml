#macro SAVE_FILE "jpmg_kernyourshit"

#macro DEBUG true
#macro VERSION 1.0

#region Game stuff

#macro LETTER_SPACING 32

#endregion

#region Colors

#macro C_EGGSHELL make_color_rgb(230, 215, 169)
#macro C_SUNLIGHT make_color_rgb(231, 194, 49)

#endregion

#region GML shorthands

#macro ICD instance_create_depth
#macro ICL instance_create_layer

#macro IDE instance_destroy

#macro IEX instance_exists

#macro KBC keyboard_check
#macro KBP keyboard_check_pressed

#endregion