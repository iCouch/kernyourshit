/// @func between(val, min, max)
/// @arg value
/// @arg min
/// @arg max
/// @desc Returns TRUE if a value is between two others

return ((argument0 >= argument1) && (argument0 <= argument2));