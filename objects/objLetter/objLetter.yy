{
    "id": "5f299d6f-dc62-43a5-a879-b969199503e5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objLetter",
    "eventList": [
        {
            "id": "aad83fd4-fb28-4006-a07a-e8288db0ccd6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5f299d6f-dc62-43a5-a879-b969199503e5"
        },
        {
            "id": "8912d897-8785-4093-a49c-1e619592908a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "5f299d6f-dc62-43a5-a879-b969199503e5"
        },
        {
            "id": "c68cc133-6302-4ee4-9d7e-fcd809f4ef1c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "5f299d6f-dc62-43a5-a879-b969199503e5"
        },
        {
            "id": "b2e24ce5-3f40-4cab-aab7-2770c6733b4b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "5f299d6f-dc62-43a5-a879-b969199503e5"
        },
        {
            "id": "55779d5f-fca1-40df-946b-dd691bf49a05",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 56,
            "eventtype": 6,
            "m_owner": "5f299d6f-dc62-43a5-a879-b969199503e5"
        }
    ],
    "maskSpriteId": "bc5e3b55-b8fd-4327-9507-2c20cecd91d2",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}