draw_set_font(fntDefault);
draw_set_color(global.boss ? make_color_hsv(current_time + irandom(10), 255, 255) : color);
draw_set_alpha(global.boss ? irandom_range(150, 255) : 1);
draw_text(global.boss ? x + random_range(-1, 1) : x, global.boss ? y + random_range(-1, 1) : y, character);

if (DEBUG)
{
	draw_set_color(c_red);
	draw_rectangle(bbox_left, bbox_top, bbox_right, bbox_bottom, true);
}