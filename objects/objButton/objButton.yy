{
    "id": "ba004e60-0fbe-4fa1-92ee-e5efdcc96b55",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objButton",
    "eventList": [
        {
            "id": "37e37913-b225-4c65-93ba-5ab04ed0b435",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ba004e60-0fbe-4fa1-92ee-e5efdcc96b55"
        },
        {
            "id": "70d8c26d-3aeb-4838-bb11-877dd25a762f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "ba004e60-0fbe-4fa1-92ee-e5efdcc96b55"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "814b78db-137f-4092-8ca1-2f1e7f4be39f",
    "visible": true
}