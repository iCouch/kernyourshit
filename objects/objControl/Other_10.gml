word = global.words[| irandom(ds_list_size(global.words) - 1)];

with (objLetter)
{
	instance_destroy();
}

ds_list_clear(characters);
ds_list_clear(positions);
ds_list_clear(offsets);
ds_list_clear(letters);

for (var i = 1; i <= string_length(word); i++)
{
	characters[| i] = string_char_at(word, i);
	positions[| i] = (i * 32) + round(room_width / 2 - (string_length(word) * 32 / 2) - 32);
	offsets[| i] = between(i, 2, string_length(word) - 1) ? ceil(random_range(-3, 3)) * 3 : 0;
	
	var a = ICD(positions[| i] + offsets[| i], room_height / 2 - (32 / 2), 0, objLetter);
	a.movable = between(i, 2, string_length(word) - 1);
	a.character = characters[| i];
	letters[| i] = a;
}

show_debug_message(word);