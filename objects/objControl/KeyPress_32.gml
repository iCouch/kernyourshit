var pn = 0;

for (var i = 2; i < string_length(word); i++)
{
	if (between(floor(letters[| i].x), positions[| i] - 2, positions[| i] + 2))
		pn += 100;
	else
		pn -= 100;
}

var p = ICD(room_width / 2, room_height / 2, -5, objPoints);
p.points = pn;

if ((global.level + 1) == 10 || global.level > 10)
	global.boss = true;
else
	global.level++;

global.scoreCurrent += pn;

event_user(0);