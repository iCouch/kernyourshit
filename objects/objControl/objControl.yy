{
    "id": "10595122-0082-4f09-82bf-329445af38dd",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objControl",
    "eventList": [
        {
            "id": "0f73faa7-0ae8-40ad-9b48-3975928e934f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "10595122-0082-4f09-82bf-329445af38dd"
        },
        {
            "id": "7e7cb6f2-5354-4a8b-a8d5-61147ceec2fa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 82,
            "eventtype": 9,
            "m_owner": "10595122-0082-4f09-82bf-329445af38dd"
        },
        {
            "id": "b1103aae-de69-4481-9d22-3cbc550d8bf1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "10595122-0082-4f09-82bf-329445af38dd"
        },
        {
            "id": "2b3d094e-0d53-4e23-a6fa-c2c15c98f387",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "10595122-0082-4f09-82bf-329445af38dd"
        },
        {
            "id": "4eba7f4f-3c7a-427b-976a-fa583229de38",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "10595122-0082-4f09-82bf-329445af38dd"
        },
        {
            "id": "e7af58ad-8253-42d1-90b2-47b09e0fa5e9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 32,
            "eventtype": 9,
            "m_owner": "10595122-0082-4f09-82bf-329445af38dd"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}