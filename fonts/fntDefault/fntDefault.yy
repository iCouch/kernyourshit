{
    "id": "65637cc8-b432-4096-98c1-5413a205d163",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fntDefault",
    "AntiAlias": 0,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Press Start 2P",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "f204d7d0-9c23-4a28-ad10-26a0e5714da9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 32,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "6242d7b9-a395-4ae7-8d7c-c8841a83b122",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 32,
                "offset": 8,
                "shift": 32,
                "w": 12,
                "x": 48,
                "y": 172
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "6bebf744-a5f7-40a1-9e0a-e5b8139ac889",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 32,
                "offset": 4,
                "shift": 32,
                "w": 20,
                "x": 288,
                "y": 138
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "3f06345f-18af-4916-a474-388482971493",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 28,
                "x": 182,
                "y": 104
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "0eecb670-607a-4061-a2ed-1293fa92f6c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 28,
                "x": 302,
                "y": 104
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "8d44c618-1fd9-4059-be2f-ebccd11a1772",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 28,
                "x": 122,
                "y": 104
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "ddb2faaf-d32c-4772-93aa-3b05284c8b60",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 28,
                "x": 62,
                "y": 104
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "f6896300-6e7e-46a5-88ed-6bbce19dbe75",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 32,
                "offset": 8,
                "shift": 32,
                "w": 8,
                "x": 62,
                "y": 172
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "3ceb3233-0409-4d9b-9244-69a0f8ecad27",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 32,
                "offset": 8,
                "shift": 32,
                "w": 16,
                "x": 420,
                "y": 138
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "ce29c01b-fc09-4a16-9b4a-f0c0883ebe27",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 32,
                "offset": 4,
                "shift": 32,
                "w": 16,
                "x": 2,
                "y": 172
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "b9ee08cc-e9db-42a1-9308-059173f234cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 28,
                "x": 422,
                "y": 70
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "fc5bfffd-0481-4a59-8327-ec2aaf071bad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 32,
                "offset": 4,
                "shift": 32,
                "w": 24,
                "x": 452,
                "y": 104
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "b1c6671c-74df-4835-ad65-3f5769db1733",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 32,
                "offset": 4,
                "shift": 32,
                "w": 12,
                "x": 34,
                "y": 172
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "aa016edd-69dc-42f7-b605-3825c2b235e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 32,
                "offset": 4,
                "shift": 32,
                "w": 24,
                "x": 54,
                "y": 138
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "cdf035d0-9515-4025-9bca-b3490663cfca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 32,
                "offset": 8,
                "shift": 32,
                "w": 8,
                "x": 102,
                "y": 172
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "75d81e72-f334-4258-b785-028c312884e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 28,
                "x": 422,
                "y": 104
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "022a10d5-9535-4116-922d-64d2745aa325",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 28,
                "x": 32,
                "y": 104
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "5c6a3c6b-8fa9-46f8-8aa7-502db9992fd2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 32,
                "offset": 4,
                "shift": 32,
                "w": 24,
                "x": 132,
                "y": 138
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "ee6b7a75-ec43-4d50-b4a7-97e7bd67b60f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 28,
                "x": 272,
                "y": 70
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "fb355550-f925-4171-a3a1-b70d79b507ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 28,
                "x": 302,
                "y": 70
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "ab845533-7422-4837-9435-eed9542edf30",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 28,
                "x": 332,
                "y": 70
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "8ed90dd8-8104-42d1-a59a-d5fad626632b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 28,
                "x": 362,
                "y": 70
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "30be8792-3c22-40c9-a009-39dce1d6cfe5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 28,
                "x": 392,
                "y": 70
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "48b4638f-5a62-4ba4-914e-cb5a5550ba13",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 28,
                "x": 452,
                "y": 70
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "45125146-615b-4947-ae14-2e581761896e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 28,
                "x": 2,
                "y": 104
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "ee5f5b35-6d6d-4e94-b481-27a1c22e4a26",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 28,
                "x": 92,
                "y": 104
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "b63bcf16-7a90-4da2-bfd3-ce0dd379dad3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 32,
                "offset": 8,
                "shift": 32,
                "w": 8,
                "x": 72,
                "y": 172
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "cb8c37f8-b51a-49e3-88c1-ffadb4f96704",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 32,
                "offset": 4,
                "shift": 32,
                "w": 12,
                "x": 20,
                "y": 172
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "2a741cd0-4290-4f1a-b852-43f8ec97f787",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 32,
                "offset": 4,
                "shift": 32,
                "w": 20,
                "x": 332,
                "y": 138
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "7b6e950a-647a-48d4-8627-c268d733f75a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 28,
                "x": 362,
                "y": 104
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "937bf662-7a9a-44ba-9c65-8d9a22c6a87f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 32,
                "offset": 4,
                "shift": 32,
                "w": 20,
                "x": 310,
                "y": 138
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "8c0a080f-a1f9-462f-8c61-c0348cf5ad83",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 28,
                "x": 332,
                "y": 104
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "6da61c4e-03e7-4d94-b6e6-83d16b491002",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 28,
                "x": 392,
                "y": 104
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "1454aa0b-9a67-4858-b75f-47a471e33127",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 28,
                "x": 272,
                "y": 104
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "91200eca-ffa3-48cb-9568-df1261e9dc22",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 28,
                "x": 242,
                "y": 104
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "276b6451-18e3-4a47-ab1f-d62ee8f2335b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 28,
                "x": 212,
                "y": 104
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "4f302384-f492-49da-acfd-9489ec3f5be7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 28,
                "x": 152,
                "y": 104
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "329d5211-9f88-4697-8e66-68da55de6c29",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 28,
                "x": 212,
                "y": 70
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "dda9d20c-934d-4096-9494-6ed285785b87",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 28,
                "x": 242,
                "y": 70
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "6eae2a01-80e7-48d3-9e18-d1722872189c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 28,
                "x": 152,
                "y": 70
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "805ee9e7-9f61-4b22-a9d2-ae81b8f5af46",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 28,
                "x": 332,
                "y": 36
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "6f791c44-ef4c-4c75-8e34-4beb1bd4b6fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 32,
                "offset": 4,
                "shift": 32,
                "w": 24,
                "x": 184,
                "y": 138
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "3e4f9048-e6b3-4b16-95bd-76734463d51f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 28,
                "x": 456,
                "y": 2
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "e5eb4f31-7aab-43e9-9dbe-fef09debf8e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 28,
                "x": 426,
                "y": 2
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "35f429db-e764-45a8-b717-1b35627f6a44",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 32,
                "offset": 4,
                "shift": 32,
                "w": 24,
                "x": 210,
                "y": 138
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "285c9459-0f69-43df-ad46-94bdce5d1bec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 28,
                "x": 396,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "02a39946-66d7-4e46-8d88-58cad439dacd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 28,
                "x": 366,
                "y": 2
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "763461bf-34e0-4981-8701-3c9ef3fea6fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 28,
                "x": 182,
                "y": 70
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "b167b53a-aba0-42cc-a5bb-58494d96b223",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 28,
                "x": 306,
                "y": 2
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "010b8d23-ee25-4b87-a732-b4fa087effe4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 28,
                "x": 276,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "9726ea1b-812e-46bc-9611-05643c8752ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 28,
                "x": 246,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "fafedc80-7b9f-4572-9a4e-a58f68c6f3e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 28,
                "x": 216,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "0eb229f4-23b0-4295-ba9a-720edab333b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 32,
                "offset": 4,
                "shift": 32,
                "w": 24,
                "x": 158,
                "y": 138
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "0072ef14-fcaa-4300-99af-491813191df0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 28,
                "x": 186,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "6bbbb7b6-5c6d-4079-83d6-d658c804f35e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 28,
                "x": 156,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "b8bf28c0-a8fe-403c-917d-5c4a552a389e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 28,
                "x": 126,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "aadc3859-84ea-495e-80df-90a0f7b7ed2e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 28,
                "x": 96,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "ac46d29f-186f-4fc0-bbd0-3e049cdb2b6b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 32,
                "offset": 4,
                "shift": 32,
                "w": 24,
                "x": 262,
                "y": 138
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "a32513a7-cad2-43da-94fe-6ac2610d2f20",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 28,
                "x": 66,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "8e07863d-f9e8-4dc7-9f7f-28aec8d15916",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 32,
                "offset": 8,
                "shift": 32,
                "w": 16,
                "x": 438,
                "y": 138
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "67ceacd8-9720-47ce-aee4-5dd92ebe3aa9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 28,
                "x": 36,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "6ecd5f10-553f-4aa7-aa5c-77ca449c5fed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 32,
                "offset": 4,
                "shift": 32,
                "w": 16,
                "x": 456,
                "y": 138
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "66a933a6-0d4f-4985-9491-83da4cedbfc6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 32,
                "offset": 4,
                "shift": 32,
                "w": 20,
                "x": 398,
                "y": 138
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "ee681ffe-052d-44f4-b079-0f378d4550bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 28,
                "x": 2,
                "y": 36
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "90c1065d-cc91-4731-8e8d-6ab5d2d57042",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 32,
                "offset": 12,
                "shift": 32,
                "w": 8,
                "x": 82,
                "y": 172
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "80812dd0-ad07-4af4-941e-5501833fc848",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 28,
                "x": 32,
                "y": 36
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "c17c8cf4-51b5-4c40-9ad6-b6e8e4a3fca2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 28,
                "x": 62,
                "y": 36
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "f716904e-e2bb-481e-b638-fd1e42720b74",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 28,
                "x": 92,
                "y": 36
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "3f9050e8-16c0-469c-9780-125c740d7a94",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 28,
                "x": 92,
                "y": 70
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "fd4f35bb-b752-47de-8971-000872dd9856",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 28,
                "x": 62,
                "y": 70
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "69517acb-e1a6-4b11-8353-8ee98aa60e00",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 32,
                "offset": 4,
                "shift": 32,
                "w": 24,
                "x": 236,
                "y": 138
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "f87898be-68c6-455d-8599-5745501b9455",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 28,
                "x": 32,
                "y": 70
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "d3a133d2-310d-42b1-96ca-3aa5fd75d472",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 28,
                "x": 2,
                "y": 70
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "70619543-7320-4af0-8396-efbb0211a521",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 32,
                "offset": 4,
                "shift": 32,
                "w": 24,
                "x": 28,
                "y": 138
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "3a762da3-ae6e-4364-b9fc-38112365e0a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 32,
                "offset": 4,
                "shift": 32,
                "w": 20,
                "x": 376,
                "y": 138
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "685c215f-7c5c-41e0-84bc-0e3ea970c228",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 28,
                "x": 452,
                "y": 36
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "780385a1-d6a5-42c7-88c6-6a6a454d29bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 32,
                "offset": 4,
                "shift": 32,
                "w": 24,
                "x": 2,
                "y": 138
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "411074e1-8bba-43e1-a394-a2d552595cee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 28,
                "x": 422,
                "y": 36
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "214435fd-40a0-42c5-a17d-f10eb385baf1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 28,
                "x": 392,
                "y": 36
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "0b7f9604-2921-469b-a9f4-f2ad400f8c92",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 28,
                "x": 122,
                "y": 70
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "f5d619c2-03bc-4f40-9e27-156c708e434b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 28,
                "x": 362,
                "y": 36
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "b4ee1f34-bdb1-471a-85ff-234dcf592ed6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 28,
                "x": 302,
                "y": 36
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "d3e5502f-fe35-445d-8dc7-b2468aadc39e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 32,
                "offset": 4,
                "shift": 32,
                "w": 24,
                "x": 106,
                "y": 138
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "bd793ef9-aa1d-477a-8207-0538dff174a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 28,
                "x": 272,
                "y": 36
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "92e92556-8640-4039-a0a6-7e33a14bcf3a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 32,
                "offset": 4,
                "shift": 32,
                "w": 24,
                "x": 478,
                "y": 104
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "4eb97c59-080a-48c5-bc5a-260ebd886279",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 28,
                "x": 242,
                "y": 36
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "33228ddb-6d45-4224-aae7-5f69763949c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 32,
                "offset": 4,
                "shift": 32,
                "w": 24,
                "x": 80,
                "y": 138
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "4816e8fc-a317-410b-8e40-2c47f3907f3a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 28,
                "x": 212,
                "y": 36
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "e0f3829c-4369-44ce-a0d6-4ecee0a22f37",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 28,
                "x": 182,
                "y": 36
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "f7b2be6d-9539-400b-b8e6-baedccfc541a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 28,
                "x": 152,
                "y": 36
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "dc88517c-0877-4f32-93c0-90e2f1fadc31",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 28,
                "x": 122,
                "y": 36
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "5c5ed3c5-3ddb-446a-9470-736ff0a74895",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 32,
                "offset": 8,
                "shift": 32,
                "w": 16,
                "x": 474,
                "y": 138
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "92ea8ea3-1cd8-40aa-b46a-589c1c6bc053",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 32,
                "offset": 12,
                "shift": 32,
                "w": 8,
                "x": 92,
                "y": 172
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "be09482f-e768-45ad-bef1-754aad0fb181",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 32,
                "offset": 4,
                "shift": 32,
                "w": 16,
                "x": 492,
                "y": 138
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "c54a1ba5-5c78-4b4e-803e-5dc246c5660e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 28,
                "x": 336,
                "y": 2
            }
        },
        {
            "Key": 127,
            "Value": {
                "id": "8b1aab5a-e11d-4f42-a96f-52a36aec63e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 127,
                "h": 32,
                "offset": 4,
                "shift": 32,
                "w": 20,
                "x": 354,
                "y": 138
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 24,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}